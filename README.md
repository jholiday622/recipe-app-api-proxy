# Recipe app api proxy

NGINX proxy app for our recipe app API

## Usage

### Enviroment Variables

* 'Listen_Port' - Port to listen on (default: '8000')
* 'App_Host' - Hostname of the app to forward request to (default: app')
* 'App_Port' - Port of the app to forward request to (defalut: '9000')
